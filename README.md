# zraminit

Simple systemd service to initialize a zram swap device at boot

## Installation:

- Open `zraminit.service` in a text editor, and in the `ExecStart` line:
    1. Change `-s 4G` to your system's memory size (`-s 2G` for 2GiB, for example)
    2. Change `-t 4` to the number of threads your CPU has (`-t 8` for a CPU with 8 threads, for example)
    3. Save and exit
- As root, run `install.sh`.
- If it worked, you should see a `/dev/zram0` swap device in `/proc/swaps`.